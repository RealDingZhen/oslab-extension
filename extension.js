// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode = require('vscode');

// This method is called when your extension is activated
// Your extension is activated the very first time the command is executed

/**
 * @param {vscode.ExtensionContext} context
 */
function activate(context) {

	// Use the console to output diagnostic information (console.log) and errors (console.error)
	// This line of code will only be executed once when your extension is activated
	console.log('Congratulations, your extension "oslab-submit" is now active!');

	// The command has been defined in the package.json file
	// Now provide the implementation of the command with  registerCommand
	// The commandId parameter must match the command field in package.json
	let submit = new vscode.ShellExecution('make submit')
	let grade = new vscode.ShellExecution('make grade')
	let debug = new vscode.ShellExecution('make qemu-gdb')
	let echo = new vscode.ShellExecution('echo hello world')
	let exit = new vscode.ShellExecution('exit $?')

	const provideTerminal = (name) => {
		let term = vscode.window.terminals.find((a) => a.name == name)
		if (term === undefined) {
			term = vscode.window.createTerminal(name)
		}
		return term
	}

	let command_submit = vscode.commands.registerCommand('oslab-submit.submit', () => {
		let term = provideTerminal('oslab')
		term.sendText(submit.commandLine, true)
		term.show()
	});

	let command_grade = vscode.commands.registerCommand('oslab-submit.grade', () => {
		let term = provideTerminal('oslab')
		term.sendText(grade.commandLine, true)
		term.show()
	})

	let command_debug = vscode.commands.registerCommand('oslab-submit.debug', () => {
		const taskName = 'kernel-debug';
		let term = provideTerminal('oslab-debug')
		term.sendText(debug.commandLine, true)
		term.show()
		vscode.debug.startDebugging(vscode.workspace.workspaceFolders[0], taskName)
	})

	context.subscriptions.push(command_submit);
	context.subscriptions.push(command_grade);
	context.subscriptions.push(command_debug);
}

// This method is called when your extension is deactivated
function deactivate() { }

module.exports = {
	activate,
	deactivate
}
