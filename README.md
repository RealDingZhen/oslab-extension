# OSLab Extension

Small tool to help my oslab.

## Commands

- OSLab Grade: to grade the lab.
- OSLab Debug: to debug the lab.
- OSLab Submit: to submit.